#!/usr/bin/env python

import argparse

from statemachine import Strategies as St


def generator():
    # Parse inputs
    parser = argparse.ArgumentParser(prog='generator', description='Table generator.')

    parser.add_argument('-s', '--src', action='store',nargs=1, help='table source path')
    args = parser.parse_args()

    # Read Table
    s = St("config.json")
    s.load_file("examples/PLANILLAROBOT.xls")
    print(s.spreadsheet)
    pass


if __name__ == "__main__":
    generator()
