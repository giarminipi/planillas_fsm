import json
import pandas as pd


class States:

    inputs_col_names = []
    outputs_col_names = []
    future_state_col_name = ""
    states = None

    # def __init__(self, dataframe_estado, inputs_col_names=[], outputs_col_names=[], future_state_col_name=[]):
    #     self.df = dataframe_estado
    #     self.inputs_col_names = inputs_col_names
    #     self.outputs_col_names = outputs_col_names
    #     self.future_state_col_name = future_state_col_name
    #     pass

    def __iter__(self):
        pass

    def __next__(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]

    def expand(self):

        pass

    def check_duplicates(self):
        pass

    def check_undefined_cases(self):
        pass



class Strategies:

    spreadsheet = pd.DataFrame()
    states = None
    fstates = None
    config = None

    def __init__(self, config=None):
        if config is None:
            self.config = "config.json"
        else:
            with open(config) as data_file:
                self.config = json.load(data_file)
        pass

    def get_states(self):
        return self.states

    def load_file(self, filename, sheet=""):
        self.spreadsheet = pd.read_excel(filename, sheet=sheet)
        self.spreadsheet = self._expand(self.spreadsheet)
        self._extract_states()
        pass

    def _expand(self, df):
        for row in df.iterrows():
            cols = row[1][row[1].isin(['X'])].keys()
            if not cols.empty:
                col = cols[0]

                s0 = row[1].copy()
                s0[col] = 0

                s1 = row[1].copy()
                s1[col] = 1

                df = df.drop(row[0])
                df = df.append([s0, s1])
                df = df.reset_index(drop=True)
                return self._expand(df)
        return df

    def _extract_states(self):
        self.states = self.spreadsheet.groupby(self.config["states"])
        self.fstates = self.spreadsheet.groupby(self.config["future_states"])

        if self._check_states_integrity() is False:
            print("Different number of states")
        pass

    # TODO Hacer una prueba de testeo para ver si funciona bien
    def _check_states_integrity(self):
        return all(state in self.states.indices.keys() for state in self.fstates.indices.keys())